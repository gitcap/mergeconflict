package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppController 
{
	@GetMapping("/")
	public String home()
	{
		return "<h1>Welcome</h1>";
	}
	@GetMapping("/user")
    public String user()
    {
    	return "WelcomeUser";
    }
	@GetMapping("/admin")
    public String admin()
    {
    	return "WelcomeAdmin";
    }
}
