package com.example.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

//@Configuration
@EnableWebSecurity
public class AppConfiguration extends WebSecurityConfigurerAdapter
{

	
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception
	{
		auth.inMemoryAuthentication()
		.withUser("surya")
		.password("$2a$10$fflycGOarlx3Fa3FW3JTv.M7EMdvptv75nQN9bOuOS0gJMzysRRa2")
		.roles("USERS")
		.and()
		.withUser("ravi")
		.password("$2a$10$hM0pQBFshSUMJm6cI9w1COaWe5nBkdUBwrAEoEWIiTzy3msXP5NBa")
		.roles("ADMIN");
		
	}
	@Bean
	public PasswordEncoder getPasswordEncoder()
	{
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception 
	{
		http.authorizeRequests()
		.antMatchers("/admin").hasRole("ADMIN")
		.antMatchers("/user").hasAnyRole("ADMIN","USERS")
		.and()
		.formLogin();
	}
	
	
	

}
